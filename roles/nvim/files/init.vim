

if !exists('g:vscode')

" NB: les réglages spécifiques à un type de fichier vont dans :
" ~/.config/nvim/after/ftplugin/filetype_name.vim

" NB: on peut paramétrer des détails de formatage de texte avec formatoptions
" ça a l'air bien, faut regarder dans l'aide

" PLUGINS MANAGER {{{
call plug#begin('~/.local/share/nvim/plugged')

" Les trucs jolis
Plug 'bling/vim-airline'
Plug 'dracula/vim',{'as':'dracula'}

" Autoclose parenthèses etc.
Plug 'jiangmiao/auto-pairs'
let g:AutoPairsMultilineClose=0

" Commentaire de ligne <leader>c<space>
Plug 'scrooloose/nerdcommenter'

" Syntax pour languages
"Plug 'stephpy/vim-yaml'
Plug 'pearofducks/ansible-vim'

" XML
Plug 'sukima/xmledit'
Plug 'Valloric/MatchTagAlways'

" Markdown
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

" VIMWIKI
Plug 'vimwiki/vimwiki'
let g:vimwiki_list = [{'path': '~/doc/vimwiki/', 'path_html': '~/doc/vimwiki_web'}]

call plug#end()


" }}}

" MAPPINGS {{{
" Loosy shift when saving
cmap Wq wq
cmap Q q
cmap W w

" Cancel search highlight
nnoremap <leader><space> :noh<cr>

" Déplacement vertical dans les lignes wrappées
nnoremap j gj
nnoremap k gk

" Space ouvre les "pliages de code mais les ferme pas. Avec ça, oui.
nnoremap <space> za

" DEPLACER des truc séléctionnés
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Remap search with dash because QWERTY-CH
vnoremap - /
nnoremap - /

" Simplification de la navigation dans l'AIDE
"    Press Enter to jump to the subject (topic) under the cursor.
"    Press Backspace to return from the last jump.
"    Press s to find the next subject, or S to find the previous subject.
"    Press o to find the next option, or O to find the previous option.

autocmd FileType help nnoremap <buffer> <CR> <C-]>
autocmd FileType help nnoremap <buffer> <BS> <C-T>
autocmd FileType help nnoremap <buffer> o /'\l\{2,\}'<CR>
autocmd FileType help nnoremap <buffer> O ?'\l\{2,\}'<CR>
autocmd FileType help nnoremap <buffer> s /\|\zs\S\+\ze\|<CR>
autocmd FileType help nnoremap <buffer> S ?\|\zs\S\+\ze\|<CR>


" }}}

" LEADER
let mapleader = "§"

" COLOR SCHEME
colorscheme dracula
set background=dark
set t_Co=256

" Execute Python script
" Shell est une fonction perso qui ouvre un buffer et colle l'output du script
" dedans. On peut ajouter <C-w> à la fin de la commande pour pouvoir
" directement fermer le buffer avec c
autocmd FileType python map <buffer> <F9> :w<CR>:Shell %:p<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" SETTINGS {{{
set mouse=a 	 		        " Enable mouse
set autowrite 			        " Save file when switching buffer
set cursorline 			        " Surligage ligne du curseur
set termguicolors	 	        " Nottament pour que les couleurs du shema fonctionnent
set backspace=indent,eol,start	" Fix backspace indent
set clipboard=unnamedplus       " Copy paste between vim and everything else Nécésite xclip
set autoindent
set scrolljump=-15              " Accelerate scrolling
set noshowmode                  " Enlève la mention --INSERT-- en dessous de la status bar
set noshowcmd                   " Ne montre pas la dernière commande dans la bare de commande
set number                      " Numérotation des lignes
set splitright                  " les split verticaux se créent sur la droite
set background=dark

" Tab shenanigans
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set smarttab

" Can undo after reopening the file
set undofile
set undodir=/tmp

" Devient sensible à la casse que si on tappe des majuscules
set ignorecase
set smartcase

" }}}

" FONCTIONS {{{
" Auto source when writing to init.vm alternatively you can run :source $MYVIMRC
au! BufWritePost $MYVIMRC source %


" Permutte entre numéros de ligne relatifs (Normal mode) et absolus (Insert mode)
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

" Ouvre un buffer et colle le résultat de l'exécution d'une commande
command! -complete=file -nargs=+ Shell call s:runshellcommand(<q-args>)
function! s:runshellcommand(cmdline)
  botright vnew
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  call setline(1,a:cmdline)
  call setline(2,substitute(a:cmdline,'.','=','g'))
  execute 'silent $read !'.escape(a:cmdline,'%#')
  setlocal nomodifiable
  1
endfunction


" }}}

au bufNewFile,BufRead *.{xsl,xslt} set filetype=xml

" Config syntax plugin Ansible
let g:ansible_attribute_highlight = "ob"
let g:ansible_name_highlight = 'd'

else

set clipboard=unnamedplus       " Copy paste between vim and everything else Nécésite xclip

function! s:manageEditorSize(...)
    let count = a:1
    let to = a:2
    for i in range(1, count ? count : 1)
        call VSCodeNotify(to == 'increase' ? 'workbench.action.increaseViewSize' : 'workbench.action.decreaseViewSize')
    endfor
endfunction

function! s:vscodeCommentary(...) abort
    if !a:0
        let &operatorfunc = matchstr(expand('<sfile>'), '[^. ]*$')
        return 'g@'
    elseif a:0 > 1
        let [line1, line2] = [a:1, a:2]
    else
        let [line1, line2] = [line("'["), line("']")]
    endif

    call VSCodeCallRange("editor.action.commentLine", line1, line2, 0)
endfunction

function! s:openVSCodeCommandsInVisualMode()
    normal! gv
    let visualmode = visualmode()
    if visualmode == "V"
        let startLine = line("v")
        let endLine = line(".")
        call VSCodeNotifyRange("workbench.action.showCommands", startLine, endLine, 1)
    else
        let startPos = getpos("v")
        let endPos = getpos(".")
        call VSCodeNotifyRangePos("workbench.action.showCommands", startPos[1], endPos[1], startPos[2], endPos[2], 1)
    endif
endfunction

function! s:openWhichKeyInVisualMode()
    normal! gv
    let visualmode = visualmode()
    if visualmode == "V"
        let startLine = line("v")
        let endLine = line(".")
        call VSCodeNotifyRange("whichkey.show", startLine, endLine, 1)
    else
        let startPos = getpos("v")
        let endPos = getpos(".")
        call VSCodeNotifyRangePos("whichkey.show", startPos[1], endPos[1], startPos[2], endPos[2], 1)
    endif
endfunction

" Better Navigation
nnoremap <silent> <C-j> :call VSCodeNotify('workbench.action.navigateDown')<CR>
xnoremap <silent> <C-j> :call VSCodeNotify('workbench.action.navigateDown')<CR>
nnoremap <silent> <C-k> :call VSCodeNotify('workbench.action.navigateUp')<CR>
xnoremap <silent> <C-k> :call VSCodeNotify('workbench.action.navigateUp')<CR>
nnoremap <silent> <C-h> :call VSCodeNotify('workbench.action.navigateLeft')<CR>
xnoremap <silent> <C-h> :call VSCodeNotify('workbench.action.navigateLeft')<CR>
nnoremap <silent> <C-l> :call VSCodeNotify('workbench.action.navigateRight')<CR>
xnoremap <silent> <C-l> :call VSCodeNotify('workbench.action.navigateRight')<CR>

nnoremap gr <Cmd>call VSCodeNotify('editor.action.goToReferences')<CR>

" Bind C-/ to vscode commentary since calling from vscode produces double comments due to multiple cursors
xnoremap <expr> <C-/> <SID>vscodeCommentary()
nnoremap <expr> <C-/> <SID>vscodeCommentary() . '_'

nnoremap <silent> <C-w>_ :<C-u>call VSCodeNotify('workbench.action.toggleEditorWidths')<CR>

nnoremap <silent> <Space> :call VSCodeNotify('whichkey.show')<CR>
xnoremap <silent> <Space> :<C-u>call <SID>openWhichKeyInVisualMode()<CR>

xnoremap <silent> <C-P> :<C-u>call <SID>openVSCodeCommandsInVisualMode()<CR>

xmap gc  <Plug>VSCodeCommentary
nmap gc  <Plug>VSCodeCommentary
omap gc  <Plug>VSCodeCommentary
nmap gcc <Plug>VSCodeCommentaryLine

" Simulate same TAB behavior in VSCode
nmap <Tab> :Tabnext<CR>
nmap <S-Tab> :Tabprev<CR>

endif

" vim:foldmethod=marker:foldlevel=0
