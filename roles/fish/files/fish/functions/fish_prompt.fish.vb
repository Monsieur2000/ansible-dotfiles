function fish_prompt

    set -q __fish_git_prompt_showupstream
    or set -g __fish_git_prompt_showupstream auto

    echo -n '┬┤'
    set_color cyan
    echo -n $USER
    set_color normal
    echo -n '@'
    set_color magenta
    echo -n (prompt_hostname)
    set_color normal
    echo -n '├'

    # git
    set prompt_git (__fish_git_prompt | string trim -c ' ()')
    test -n "$prompt_git"
    and echo -n '┤' G $prompt_git '├'




    echo -n '┤'
    set_color green
    echo -n (prompt_pwd)
    set_color normal
    echo
    echo '╰─$ '





end
