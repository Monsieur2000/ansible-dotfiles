# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# ~ # VARIABLES
set $mod Mod4
set $term --no-startup-id $TERMINAL

# Set Xresources colors:
set_from_resource $darkblack    background #000000
set_from_resource $black        color8  #000000
set_from_resource $darkred      color1  #000000
set_from_resource $red          color9  #000000
set_from_resource $darkgreen    color2  #000000
set_from_resource $green        color10 #000000
set_from_resource $darkyellow   color3  #000000
set_from_resource $yellow       color11 #000000
set_from_resource $darkblue     color4  #000000
set_from_resource $blue         color12 #000000
set_from_resource $darkmagenta  color5  #000000
set_from_resource $magenta      color13 #000000
set_from_resource $darkcyan     color6  #000000
set_from_resource $cyan         color14 #000000
set_from_resource $darkwhite    color7  #000000
set_from_resource $white        color15 #000000

set $transparent #00000000


# ~ # CLAVIER

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill

# Focus
bindsym $mod+h focus left
bindsym $mod+Left focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Déplacer les fenêtres
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#bindsym $mod+Shift+h move left
#bindsym $mod+Shift+j move down
#bindsym $mod+Shift+k move up
#bindsym $mod+Shift+l move right

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# Scratchpad
bindsym $mod+m move scratchpad
bindsym $mod+o scratchpad show

# changer orientation du split des fenêtres
bindsym $mod+s split toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
# bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# move current workspace to another display (no need left, if only two dsiplay)
bindsym $mod+Shift+x move workspace to output right

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# ~ # WORKSPACES

# Placement multi écran
workspace 1 output DP-0
workspace 2 output DP-0
workspace 3 output DP-0
workspace 4 output DP-0
workspace 5 output DP-0
workspace 6 output DVI-D-0
workspace 7 output DVI-D-0
workspace 8 output DVI-D-0
workspace 9 output DVI-D-0
workspace 10 output DVI-D-0

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace and follow it
# attention, coller le ; à la première instruction sinon il considère que le nom du workspace se fini par une espace
bindsym $mod+Shift+1 move container to workspace 1; workspace 1
bindsym $mod+Shift+2 move container to workspace 2; workspace 2
bindsym $mod+Shift+3 move container to workspace 3; workspace 3
bindsym $mod+Shift+4 move container to workspace 4; workspace 4
bindsym $mod+Shift+5 move container to workspace 5; workspace 5
bindsym $mod+Shift+6 move container to workspace 6; workspace 6
bindsym $mod+Shift+7 move container to workspace 7; workspace 7
bindsym $mod+Shift+8 move container to workspace 8; workspace 8
bindsym $mod+Shift+9 move container to workspace 9; workspace 9
bindsym $mod+Shift+0 move container to workspace 10; workspace 10

# switcher entre les workspaces de l'écran
bindsym $mod+Tab workspace next_on_output

# ~ # START APPLICATIONS

# ATTENTION i3 ne lit pas le $PATH de .bashrc quand il doit lancer des scripts.
# i.e. les scripts perso ne seront pas trouvés a moins de l'indiquer dans .xsessionrc

# start a terminal
bindsym $mod+Return exec $term

# start terminal in same dir than the current window
bindsym $mod+Shift+Return exec samedir

# General dropdown window traits. The order can matter.
for_window [instance="dropdown_*"] floating enable
for_window [instance="dropdown_*"] move scratchpad
for_window [instance="dropdown_*"] sticky enable
for_window [instance="dropdown_*"] scratchpad show
for_window [instance="dropdown_tmuxdd"] resize set 625 450
for_window [instance="dropdown_ncmpcpp"] resize set 1600 1000
for_window [instance="dropdown_dropdowncalc"] resize set 800 300
for_window [instance="dropdown_tmuxdd"] border pixel 3
for_window [instance="dropdown_dropdowncalc"] border pixel 2
for_window [instance="dropdown_*"] move position center

# Autres
#for_window [class="VirtualBox"] floating enable
for_window [class = "Wpg"] floating enable

for_window [class="mpv"] floating enable
for_window [class="mpv"] resize set 800 600
for_window [class="mpv"] move position center

# ~ # MENUS

# exit i3 (logs you out of your X session)
#bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+Shift+e mode "$mode_system"
set $mode_system (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    bindsym s exec --no-startup-id ~/.config/i3/i3exit suspend, mode "default"
    bindsym u exec --no-startup-id ~/.config/i3/i3exit switch_user, mode "default"
    bindsym e exec --no-startup-id ~/.config/i3/i3exit logout, mode "default"
    bindsym h exec --no-startup-id ~/.config/i3/i3exit hibernate, mode "default"
    bindsym r exec --no-startup-id ~/.config/i3/i3exit reboot, mode "default"
    bindsym Shift+s exec --no-startup-id ~/.config/i3/i3exit shutdown, mode "default"

    # exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym eacute resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"


# ~ # APPARENCE

# police
font pango:Monospace 9

# couleurs
#                       BORDER          BACKGROUND  TEXT        INDICATOR       CHILD_BORDER
client.focused_inactive          $darkgreen      $green      $darkblack      $darkmagenta    $darkgreen
client.unfocused        $black       $black       $white      $darkblack       $black
client.focused $darkyellow       $yellow       $black      $darkyellow       $yellow
client.urgent           $darkred        $red        $black      $darkred        $darkred
client.background $black


# ~ # STATUS BAR
bar {
    status_command i3blocks
    colors {
            background $darkblack
            statusline $white
            separator  $cyan

            #                   bord            background      text
            focused_workspace   $green         $darkgreen     $darkblack
            active_workspace    $yellow           $darkyellow       $darkblack
            inactive_workspace  $darkblack      $darkblack      $white
            urgent_workspace    $darkred      $darkred      $darkblack
        }
}

# ~ # DEMARRAGE DES SERVICES
exec --no-startup-id $HOME/.config/screenlayout.sh
# exec --no-startup-id pulseaudio --kill
# exec --no-startup-id pulseaudio --start
exec --no-startup-id setbg # set wallpaper avec l'appli xwallpaper et le script de Luke Smith
# exec compton -b # composite manager
exec --no-startup-id sxhkd
exec --no-startup-id numlockx on
exec --no-startup-id redshift
