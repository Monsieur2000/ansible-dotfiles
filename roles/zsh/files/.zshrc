source $HOME/.config/zsh/antigen.zsh
antigen init $HOME/.config/zsh/antigenrc


autoload -U compinit && compinit # reload completions for zsh-completions

# Load shortcut aliases
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"

# Load aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

# Completion
compdef config=git

# Options
setopt HIST_IGNORE_ALL_DUPS

# Spaceship-prompt customization
SPACESHIP_PROMPT_ORDER=(
dir             # Current directory section
user            # Username section
host            # Hostname section
git             # Git section (git_branch + git_status)
time          # Time stampts section
# hg            # Mercurial section (hg_branch  + hg_status)
# package       # Package version
# node          # Node.js section
# ruby          # Ruby section
# elixir        # Elixir section
# xcode         # Xcode section
# swift         # Swift section
# golang        # Go section
# php           # PHP section
# rust          # Rust section
# haskell       # Haskell Stack section
# julia         # Julia section
# docker        # Docker section
# aws           # Amazon Web Services section
# venv          # virtualenv section
# conda         # conda virtualenv section
# pyenv         # Pyenv section
# dotnet        # .NET section
# ember         # Ember.js section
# kubecontext   # Kubectl context section
exec_time       # Execution time
line_sep        # Line break
#battery         # Battery level and status
vi_mode         # Vi-mode indicator
jobs            # Background jobs indicator
# exit_code     # Exit code section
char            # Prompt character
)

SPACESHIP_DIR_PREFIX="%{$fg[blue]%}┌─[%b "
SPACESHIP_DIR_SUFFIX="%{$fg[blue]%} ] "
SPACESHIP_CHAR_SYMBOL="%{$fg[blue]%}└─▪%b "

SPACESHIP_USER_SHOW=always
SPACESHIP_USER_SUFFIX=""
SPACESHIP_HOST_SHOW=always
SPACESHIP_HOST_PREFIX="@"

# Banière de bienvenue (police : ANSI Shadow)
if [ "$(tput cols)" -gt 75 ]; then
echo ""
echo " ██████╗ ██╗███████╗███╗   ██╗██╗   ██╗███████╗███╗   ██╗██╗   ██╗███████╗"
echo " ██╔══██╗██║██╔════╝████╗  ██║██║   ██║██╔════╝████╗  ██║██║   ██║██╔════╝"
echo " ██████╔╝██║█████╗  ██╔██╗ ██║██║   ██║█████╗  ██╔██╗ ██║██║   ██║█████╗  "
echo " ██╔══██╗██║██╔══╝  ██║╚██╗██║╚██╗ ██╔╝██╔══╝  ██║╚██╗██║██║   ██║██╔══╝  "
echo " ██████╔╝██║███████╗██║ ╚████║ ╚████╔╝ ███████╗██║ ╚████║╚██████╔╝███████╗"
echo " ╚═════╝ ╚═╝╚══════╝╚═╝  ╚═══╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝"


else
	echo -e "\t╔══════════════╗"
	echo -e "\t║  Bienvenue ! ║"
	echo -e "\t╚══════════════╝"
fi
#lecture de os-release (pour récup du nom de l'OS)
. /etc/os-release

echo ""
echo -e "\tOS................ : ${PRETTY_NAME}"
echo -e "\tHostname.......... : ${HOST}"
echo -e "\tUptime............ : $(uptime -p)"
echo -e "\tProcesses......... : $(ps -e | wc -l) total running wich $(ps -u $USER | wc -l) are yours"
echo -e "\tUsers logged...... : $(users | tr ' ' '\n' | sort -u)" # ajouter | wc -l pour avoir juste le nombre


