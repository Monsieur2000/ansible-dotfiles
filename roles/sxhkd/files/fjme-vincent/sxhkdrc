#
# ███████╗██╗  ██╗██╗  ██╗██╗  ██╗██████╗ ██████╗  ██████╗
# ██╔════╝╚██╗██╔╝██║  ██║██║ ██╔╝██╔══██╗██╔══██╗██╔════╝
# ███████╗ ╚███╔╝ ███████║█████╔╝ ██║  ██║██████╔╝██║
# ╚════██║ ██╔██╗ ██╔══██║██╔═██╗ ██║  ██║██╔══██╗██║
# ███████║██╔╝ ██╗██║  ██║██║  ██╗██████╔╝██║  ██║╚██████╗
# ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝
#
#       Author............ : Monsieur2000
#       Version........... : Avec BSPWM bindkeys
#       Date.............. : 2021-01-13
#

####################
# ~ # BASIQUES # ~ #
####################

# Terminal
super + Return
    $TERMINAL

# Mur de Terminaux
super + shift + Return
  bspc desktop -f 7 ; bspc node -f biggest.local ; $TERMINAL

# Launcher
super + d
   rofi -show run

#############################
# ~ # BSPWM SPECIFIQUES # ~ #
#############################

# Fermer fenêtre
super + shift + q
    bspc node -c

# Restart BSPWM
super + shift + r
    bspc wm -r

# Menu de fermeture
super + shift + e ; {l,e,s,h,r,shift + s}
    bspwm_exit {lock,logout,suspend,hibernate,reboot,shutdown}

# focus the node in the given direction
#super + {_,shift + }{Left,Down,Up,Right}
#	bspc node -{f,s} {west,south,north,east}

# Se déplacer entre les fenêtres
super + {j,k}
    bspc node -f {"next.local.window","prev.local.window"}

# Se déplacer entre les écrans
super + {h,l}
    bspc monitor -f {next,prev}

# Déplacer les fenêtres
super + shift + {h,j,k,l}
    bspwm_smart_move {west,south,north,east}

# focus desktop / send window to desktop
# NB: on donne l'ordre des descktop. Ainsi, si le desktop no 4 est en deuxième position (suite à des swaps), on fait super + 2 pour l'atteindre.
# Pour changer, il faut enlever le circonflexe et remplacer 10 par 0.
super + {_,shift + }{1-9,0}
    bspc {desktop -f,node -d} ^{1-9,10}

# toggle floating window
super + shift + space
    if [ -z "$(bspc query -N -n focused.floating)" ]; then \
        bspc node focused -t floating; \
    else \
        bspc node focused -t tiled; \
    fi

# resize
super + r : {Left,Down,Up,Right}
    bspwm_resize {west,south,north,east} 50

# Focus next/previous window
alt + Tab
	bspc node --focus last

# Focus next/previous desktop
super + {_,shift + }Tab
	bspc desktop --focus {next.occupied,prev.occupied}

# Rotate desktop
super + {_,shift + }c
	bspc node @/ --rotate {90,-90}

# Move window to left/right monitor
super + x
	bspc node --to-monitor next --focus

# Swap les deux écrans actifs
# NB : c'est mieux de swapper les desktop que de les déplacer car après on se retrouve avec une répartition des desktops différents du départ.
super + shift + x
    bspc desktop --swap 'next.active.!local'

# Echange desktop actif avec un vide de l'autre écran
super + shift + BackSpace
    bspc desktop --swap 'next.!local.!occupied' -a

# Ferme toutes les fenêtres du bureau actif
super + ctrl + BackSpace
    for w in $(bspc query -N -n .window -d focused); do bspc node $w -c ; done

# Swap with biggest node entre les deux écrans
super + m
    bspc node --swap biggest.active --follow

# Mettre dans Scratch pad
super + shift + w
	id=$(bspc query -N -n "focused");\
	if [ -n "$id" ];then \
		xprop -id $id -f _SCRATCH 32ii -set _SCRATCH $(date +%s,%N);\
		bspc node -t "floating";\
		xdotool windowunmap $id;\
	fi

# Faire tourner le Scratch pad
super + w
    bspwm_show_scratchpad

# Toggle  window Sticky
super + shift + s
    bspc node -g sticky || bspc node -g sticky=off

# Toggle full screen
super + f
    bspc node -t \~fullscreen

# preselect the direction or insert again to cancel the preselection.
super + ctrl + {Left,Down,Up,Right}
	bspc node -p '~{west,south,north,east}'

# preselect the ratio (chiffres du clavier pas du Keypad)
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# change window gap du Desktop courrant at run time
super + ctrl + KP_Add
    bspc config -d focused window_gap $((`bspc config -d focused window_gap` + 5 ))

# Restore original gap
super + ctrl + equal
    bspc config -d focused window_gap 6

# change window gap du Desktop courrant at run time
super + ctrl + KP_Subtract
    bspc config -d focused window_gap $((`bspc config -d focused window_gap` - 5 ))

########################
# ~ # APPLICATIONS # ~ #
########################

# Liste des raccourcis
# FIXME: faudrait générer le terminal après le choix dans rofi, modifier le script doc_raccourcis
super + F1
   $TERMINAL -t Raccourcis --class flt_raccourcis -e doc_raccourcis

super + F4
    subl

super + F5
    firefox

super + F8
   $TERMINAL --class flt_ncmpcpp -e ncmpcpp

super + shift + F7
    vboxmanage startvm "Windows 7"

# Calculatrice
super + a
#    rofi -show calc -modi calc -no-show-match -no-sort
    $TERMINAL -e "R -q --no-save"

# Trouver et ouvrir un fichier du dossier Documents
super + o
    rofi-open


######################
# ~ # MULTIMEDIA # ~ #
######################

# Playlist en cours
super + shift + F8
    mpc play `mpc -f "%position%) %artist% - %title%" playlist | rofi -dmenu -p 'morceau'| cut -d')' -f1`

# Start ProjetcM
super + ctrl + F8
    projectM-pulseaudio

# vidéos ASMR
super + shift + F9
   mpv --geometry=800x600 --input-ipc-server=/tmp/mpvsoc$(date +%s) `du -a ~/art/asmr/ | cut -f2- | rofi -matching normal -dmenu -width 80 -lines 25 -i -p 'ASMR'`

# Pause vlc, mpv, mpd, cmus en même temps
super + Pause
    dbus-send --type=method_call --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause ; mpc pause ; pauseallmpv ; cmus-remote -U

# volume casque bluetouth
super + F11
    pactl set-sink-volume bluez_sink.04_5D_4B_C0_F9_0A.a2dp_sink +5%

super + F12
    pactl set-sink-volume bluez_sink.04_5D_4B_C0_F9_0A.a2dp_sink -5%

# volume Pulse
XF86AudioRaiseVolume
    amixer set -c 0 Master 5%+
#    pulsemixer --change-volume +3

XF86AudioLowerVolume
    amixer set -c 0 Master 5%-
#    pulsemixer --change-volume -3
XF86AudioMute
    amixer set -c 0 Master toggle
#    pulsemixer --toggle-mute

# commandes mpd
XF86AudioPrev
    mpc prev
XF86AudioNext
    mpc next
XF86AudioPlay
    mpc toggle
XF86AudioStop
    mpc stop

# Mute micro
super + Print
    amixer set Capture toggle
