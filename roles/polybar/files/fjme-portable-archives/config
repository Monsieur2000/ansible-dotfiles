[settings]
screenchange-reload = false
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 2
margin-bottom = 0

[colors]
;background = ${xrdb:color0:#222}
background = #282a36
background-alt = #44475a
;foreground = ${xrdb:color7:#222}
foreground = #f8f8f2
foreground-alt = #6272a4
primary = #50fa7b
secondary = #f1fa8c
alert = #ff5555
bleu = #8BE9FD

[bar/main]
monitor = ${env:MONITOR:}

width = 100%
height = 20

fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 1.5
line-color = #f00

; bordure extérieure de la barre. La couleur a un code qui prend en compte la transparence d'où les deux 0 de plus.
border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 1

;font-0 = Inconsolata Nerd Font Mono:weight=bold:size=11;2
font-0 = TerminessTTF Nerd Font Mono:size=10;3

modules-left = bspwm xwindow
;modules-center = xwindow
modules-right = filesystem memory cpu load_avg wlan eth battery date

tray-position = right
tray-padding = 2

bottom = true

; masque polybar quand une fenêtre est en mode fullscreen
wm-restack = bspwm

[module/bspwm]
type = internal/bspwm

; Only show workspaces defined on the same output as the bar
; NOTE: The bspwm and XRandR monitor names must match, which they do by default.
; But if you rename your bspwm monitors with bspc -n this option will no longer
; behave correctly.
; Default: true
pin-workspaces = true

; Output mode flags after focused state label
; Default: false
inline-mode = false

; Create click handler used to focus workspace
; Default: true
enable-click = false

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = false

; Set the scroll cycle direction
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = false

label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 1

label-occupied = %name%
label-occupied-padding = 1

label-urgent = %name%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = %name%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 1

label-dimmed-focused-underline = ${colors.secondary}

;label-separator = |
;
[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

format-mounted-prefix = "HD "
format-mounted-prefix-foreground = ${colors.foreground-alt}
label-mounted = %free% free

label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "CPU "
format-prefix-foreground = ${colors.foreground-alt}
;format-underline = #f90000
label = %percentage:2%%

[module/load_avg]
type = custom/script
exec = uptime | grep -ohe 'load average[s:][: ].*' | awk '{ print $3 }' | sed 's/,//g'
interval = 30

format-prefix = "LOAD "
format-prefix-foreground = ${colors.foreground-alt}

label = %output%

[module/memory]
type = internal/memory
interval = 2
format-prefix = "MEM "
format-prefix-foreground = ${colors.foreground-alt}
;format-underline = #4bffdc
;!! On ne peut pas utiliser les variables de couleur à l'intérieur des labels
label = %{F#8BE9FD}%gb_used%%{F-}/%gb_total%

[module/wlan]
type = internal/network
interface = wlp0s20f3
interval = 3.0

format-connected = <label-connected>
format-connected-underline = ${colors.primary}
label-connected = %essid% | %local_ip% | DL %downspeed% UL %upspeed%

format-disconnected = wifi down
;format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.alert}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = eno1
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 20

date = "%Y-%m-%d"

time = %H:%M

label = %date% | %{F#FFB86C}%time%

[module/battery]
type = internal/battery

full-at = 99

battery = BAT0
adapter = AC

time-format = %H:%M

label-charging = %percentage%% (%time%)

label-discharging = %percentage%% (%time%)
label-discharging-underline = ${colors.alert}
label-full = FULL

format-charging-prefix = "AC "
format-charging-prefix-foreground = ${colors.foreground-alt}
format-charging-prefix-underline = ${colors.primary}

format-discharging-prefix = "BAT "
format-discharging-prefix-foreground = ${colors.foreground-alt}
format-discharging-prefix-underline = ${colors.alert}
; vim:ft=dosini
